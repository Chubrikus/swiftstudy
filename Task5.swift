class Human {
  var name: String
  var age: Int
  var sex: String
  var workExperience: Int

  init(name: String, age: Int, sex: String, workExperience: Int) {
        self.name = name
        self.age = age
        self.sex = sex
        self.workExperience = workExperience
    }
}

class Programmer : Human {
  var jobTitle: String?
  var programmingLanguage: String?
}

class Painer : Human {
  var style: String?
  var genre: String?
}

/////////

class House {
    var width: Double
    var height: Double
    
    init(width: Double, height: Double) {
        self.width = width
        self.height = height
    }
    
    func create() {
        let area = width * height
        print("House is created, area = \(area)")
    }
    
    func destroy() {
        print("House is destroyed")
    }
}

/////////

class Student {
    var name: String
    var grade: Int
    
    init(name: String, grade: Int) {
        self.name = name
        self.grade = grade
    }
    
    static func sortByName(_ students: [Student]) -> [Student] {
        return students.sorted { $0.name < $1.name }
    }
    
    static func sortByGrade(_ students: [Student]) -> [Student] {
        return students.sorted { $0.grade > $1.grade }
    }
}

/////////

// Структура "Person" с двумя свойствами
struct Person {
    var name: String
    var age: Int
}

// Класс "Man" с двумя свойствами и методом
class Dog {
  var name: String
  var age: Int

  init(name: String, age: Int) {
        self.name = name
        self.age = age
    }

   func sit() {
        print("The \(self.name) sat down")
    }
}
//Основное отличие между структурами и классами заключается в том, 
//что структуры - это значения, а классы - это ссылки. Также 
//структуры не могут быть унаследованы, и им не нужен инициализатор

/////////

import Foundation

// Определяем типы карт
enum CardRank: Int, CaseIterable {
    case two = 2, three, four, five, six, seven, eight, nine, ten
    case jack, queen, king, ace
}

enum CardSuit: String, CaseIterable {
    case spades, hearts, diamonds, clubs
}

// Создаем структуру для карты
struct Card: CustomStringConvertible {
    let rank: CardRank
    let suit: CardSuit

    var description: String {
        return "\(rank) of \(suit)"
    }
}

// Создаем колоду
var deck = [Card]()
for suit in CardSuit.allCases {
    for rank in CardRank.allCases {
        deck.append(Card(rank: rank, suit: suit))
    }
}

// Перемешиваем колоду
deck.shuffle()

// Выбираем 5 карт
let hand = Array(deck[0..<5])

// Выводим комбинацию карт
if Set(hand.map { $0.rank }).count == 1 {
    if hand.map({ $0.rank }).contains(.ace) {
        print("Royal Flush!")
    } else {
        print("Straight Flush!")
    }
} else if Set(hand.map { $0.suit }).count == 1 {
    print("Flush!")
} else if Set(hand.map { $0.rank }).count == 2 {
    print("Four of a kind!")
} else if Set(hand.map { $0.rank }).count == 3 {
    if hand.filter({ $0.rank == hand[0].rank }).count == 3 || hand.filter({ $0.rank == hand[1].rank }).count == 3 || hand.filter({ $0.rank == hand[2].rank }).count == 3 {
        print("Three of a kind!")
    } else {
        print("Two pairs!")
    }
} else if Set(hand.map { $0.rank }).count == 4 {
    print("One pair!")
} else {
    print("High card!")
}

// Выводим выбранные карты
print(hand)

