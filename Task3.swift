import Foundation

func sort(numbers: [Int], ascending: Bool) -> [Int] {
    let sorted = numbers.sorted(by: { (num1: Int, num2: Int) -> Bool in
        return ascending ? num1 < num2 : num1 > num2
    })
    return sorted
}

let numbers = [3, 6, 1, 8, 2, 9, 4, 5, 7]
let sortedAscending = sort(numbers: numbers, ascending: true)
let sortedDescending = sort(numbers: numbers, ascending: false)

print(sortedAscending)
print (sortedDescending)

/////////////

func sortFriendsByLength(names: String...) -> [String] {
    var friends = Array(names)
    friends.sort { $0.count < $1.count }
    return friends
}

let sortedFriends = sortFriendsByLength(names: "Yan", "Marina", "Igor", "Alexander", "Alice")
print(sortedFriends)

/////////////

let friends = sortedFriends

var dict = [Int: String]()
for friend in friends {
    dict[friend.count] = friend
}

func printValue(forKey key: Int) {
    if let value = dict[key] {
        print("Key: \(key), Value: \(value)")
    } else {
        print("No value found for key \(key)")
    }
}

printValue(forKey: 4)

////////////

func checkAndFill(stringArray: [String], numberArray: [Int]) {
    var newStringArray = stringArray
    var newNumberArray = numberArray
    
    if stringArray.isEmpty {
        newStringArray.append("Hello to the code viewer")
        print("String Array: \(newStringArray)")
    } else {
        print("String Array: \(stringArray)")
    }
    
    if numberArray.isEmpty {
        newNumberArray.append(0)
        print("Number Array: \(newNumberArray)")
    } else {
        print("Number Array: \(numberArray)")
    }
}

let stringArray: [String] = []
let numberArray: [Int] = [1, 2, 3]
checkAndFill(stringArray: stringArray, numberArray: numberArray)