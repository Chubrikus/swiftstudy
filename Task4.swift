enum Month: Int {
    case january = 1
    case february = 2
    case march = 3
    case april = 4
    case may = 5
    case june = 6
    case july = 7
    case august = 8
    case september = 9
    case october = 10
    case november = 11
    case december = 12
}

enum Day: String {
    case sunday = "Sunday"
    case monday = "Monday"
    case tuesday = "Tuesday"
    case wednesday = "Wednesday"
    case thursday = "Thursday"
    case friday = "Friday"
    case saturday = "Saturday"
}

/////////

enum Gender {
    case male
    case female
    case other
}

enum AgeCategory: Int {
    case young
    case youngAdult
    case adult
    case elderly
}

enum WorkExperience {
    case junior
    case middle
    case senior
}

/////////

enum RainbowColor {
    case red
    case orange
    case yellow
    case green
    case blue
    case indigo
    case violet
}

/////////

func printEnumCases (color: RainbowColor) {
    let animals = ["Ladybug", "Fox", "Giraffe", "Frog", "Whale", "Butterfly", "Starfish"]
switch color {
        case .red:
            print("\(animals[0]) red")
        case .orange:
            print("\(animals[1]) orange")
        case .yellow:
            print("\(animals[2]) yellow")
        case .green:
            print("\(animals[3]) green")
        case .blue:
            print("\(animals[4]) blue")
        case .indigo:
            print("\(animals[5]) indigo")
        case .violet:
            print("\(animals[6]) violet")
    }
}

printEnumCases(color : RainbowColor.blue)

/////////

enum Score: Int {
    case two = 2
    case three = 3
    case four = 4
    case five = 5
    
    func getDescription() -> String {
        switch self {
        case .two:
            return "неудовлетворительно"
        case .three:
            return "удовлетворительно"
        case .four:
            return "хорошо"
        case .five:
            return "отлично"
        }
    }
}

print(Score.five.getDescription())

/////////

enum MyGarage {
    case bmw
    case audi
    case mercedes
    case toyota
}


func printCars(car : MyGarage) {
        switch car {
        case .bmw:
             print("BMW in garage")
        case .audi:
            print("Audi in garage")
        case .mercedes:
            print("Mercedes in garage")
        case .toyota:
            print("Toyota in garage")
        }
}

printCars(car : MyGarage.bmw)

