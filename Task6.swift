import Foundation

struct IOSCollection {
    var number = 1
}

class Ref<T> { 
    var value : T
    init (value: T) {
        self.value = value
    } 
}

struct Container <T> { 
    var ref : Ref<T> 
    init(value: T) {
        self.ref = Ref(value: value) 
    }

    var value : T {
        get { 
          return ref.value
        }
      

        set { 
        // Если есть уникальная ссылка на объект, то меняем значение ссылки, если нет, то создаем новый экземпляр
            guard(isKnownUniquelyReferenced(&ref)) else { 

                ref = Ref(value: newValue)
                return
            }
            ref.value = newValue
        }
    }
}
var id = IOSCollection()
var container1 = Container(value: id)
var container2 = container1

func address(of object : UnsafeRawPointer) -> String {
  let addr = Int(bitPattern: object)
  return String(format: "%", addr)
}

func address(off value: AnyObject) -> String {
  return "\(Unmanaged.passUnretained(value).toOpaque())"
}

print(address(off: container1.ref))
print(address(off: container2.ref))

container2.value.number = 2

print(address(off: container1.ref))
print(address(off: container2.ref))

/////////

protocol Hotel {
    init(roomCount: Int)
}

class HotelAlfa: Hotel {
    var roomCount: Int
    
    required init(roomCount: Int) {
        self.roomCount = roomCount
    }
}

/////////

protocol GameDice {
    var numberDice: Int { get }
}

extension Int: GameDice {
    var numberDice: Int {
        print("Выпало \(self) на кубике")
        return self
    }
}

let diceCoub = 20
diceCoub.numberDice

/////////

@objc protocol Flowers {
    var name: String { get }
    @objc optional var color: String { get }
    func smell()
}

class Iris: Flowers {
    var name: String

    init(name: String) {
        self.name = name
    }

    func smell() {
        print("This \(name) smells good!")
    }
}

let iris = Iris(name: "Pygmy Iris")
iris.smell()

/////////

protocol Coding {
    var time: Int { get set }
    var codeLines: Int { get set }
    func writeCode(platform: String, numberOfSpecialist: Int)
}

protocol StopWorking {
    func stopCoding()
}

class Company: Coding, StopWorking {
    var numberOfProgrammers: Int
    var Specialists: String
    
    var time: Int = 0
    var codeLines: Int = 0
    
    init(numberOfProgrammers: Int, Specialists: String) {
        self.numberOfProgrammers = numberOfProgrammers
        self.Specialists = Specialists
    }
    
    func writeCode(platform: String, numberOfSpecialist: Int) {
      if numberOfSpecialist == 0 {
        print("Недостаточно специалистов")
      } else{
        print("Разработка началась. Пишем код для \(platform) платформы")
        }
    }
    
    func stopCoding() {
        print("Работа закончена. Сдаю в тестирование.")
    }
}

let company = Company(numberOfProgrammers: 10, Specialists: "iOS")
company.writeCode(platform: "iOS", numberOfSpecialist: company.numberOfProgrammers)