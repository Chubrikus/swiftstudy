import Foundation

let daysInMonths = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
let monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]

for days in daysInMonths {
    print("\(days)")
}

////////////////////////

for (index, days) in daysInMonths.enumerated() {
    let monthName = monthNames[index]
    print("\(monthName): \(days)")
}

////////////////////////

let tuples = (month: monthNames, days: daysInMonths)

for i in 0..<tuples.month.count {
    print("\(tuples.month[i]): \(tuples.days[i])")
}

////////////////////////

for i in (1...tuples.month.count) {
    let index = tuples.month.count - i 
    print("\(tuples.month[index]): \(tuples.days[index])")
}

////////////////////////
//Эмитируем пользовательский ввод
let monthText = "October"
let day = 21

var daysCount = 0
var monthIndex = 0
var foundMonth = false

for i in 0..<monthNames.count {
    if monthNames[i] == monthText {
        foundMonth = true
    }
    
    if !foundMonth {
        daysCount += daysInMonths[i]
        monthIndex += 1
    }
}

daysCount += day
print(daysCount)